import React from 'react'
// @ts-ignore
import { createBrowserHistory } from 'history'
import { createTheme, CssBaseline, ThemeProvider } from '@material-ui/core'
import { Route, Router, Switch } from 'react-router-dom'
import { ReactNotifications } from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import HomePage from './pages/HomePage'
import SharePage from './pages/SharePage'
import { Provider as LoginProvider } from './contexts/login'

export const history = createBrowserHistory()
const theme = createTheme()

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <ReactNotifications />
      <LoginProvider>
        <Router history={history}>
          <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route exact path="/share">
              <SharePage />
            </Route>
            <Route path="*">
              <HomePage />
            </Route>
          </Switch>
        </Router>
      </LoginProvider>
    </ThemeProvider>
  )
}

export default App
