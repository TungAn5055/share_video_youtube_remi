import { UPDATE_AFTER_LOGIN } from './login.constants'
import { handleToken } from './login.helper'

export const updateAfterLogin = (dispatch: any) => (token: any, email: any) => {
  dispatch({
    type: UPDATE_AFTER_LOGIN,
    payload: { token, email },
  })
}
