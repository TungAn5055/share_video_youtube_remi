import { UPDATE_AFTER_LOGIN } from './login.constants'

export const initialState = {
  token: null,
  email: null,
}

export const loginReducers = (state = initialState, action: any) => {
  switch (action.type) {
    case UPDATE_AFTER_LOGIN:
      return {
        ...state,
        token: action.payload.token,
        email: action.payload.email,
      }
    default:
      return state
  }
}
