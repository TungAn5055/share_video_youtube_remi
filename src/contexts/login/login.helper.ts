import { localStorageService } from '../../services/localStorage.services'
import { EMAIL_LOGIN, TOKEN } from '../../ultils/dataUtil'

export function handleToken(token: any, email: any) {
  localStorageService.setDataLocalStorage(EMAIL_LOGIN, email)
  localStorageService.setDataLocalStorage(TOKEN, token)
}
