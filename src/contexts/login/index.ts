import { initialState, loginReducers } from './login.reducers'
import { updateAfterLogin } from './login.actions'
import CreateDataContext from '../CreateDataContext'

export const { Provider, Context } = CreateDataContext(
  loginReducers,
  { updateAfterLogin },
  initialState,
)
