import { Container, Grid } from '@material-ui/core'
import React from 'react'
import Header from '../components/Header'
import ListVideo from '../components/ListVideo'

const HomePage = (props: any) => {
  return (
    <>
      <Container maxWidth="lg">
        <Header />
        <div
          style={{
            // marginTop: '100px',
            // display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            padding: '20px 90px 20px 90px',
          }}
        >
          <ListVideo />
        </div>
      </Container>
    </>
  )
}

export default HomePage
