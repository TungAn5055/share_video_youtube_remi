import { createRenderer } from 'react-test-renderer/shallow'
import React from 'react'
import HomePage from '../HomePage'

const shallowRenderer = createRenderer()
shallowRenderer.render(<HomePage />)
const renderedOutput = shallowRenderer.getRenderOutput()

describe('HomePage component', () => {
  it('should render and match the snapshot', () => {
    expect(renderedOutput).toMatchSnapshot()
  })

  it('should includes some child components', () => {
    shallowRenderer.render(<HomePage />)

    expect(renderedOutput.type).toBe('div')
    // expect(renderedOutput.props.children.length).toEqual(1)
  })
})
