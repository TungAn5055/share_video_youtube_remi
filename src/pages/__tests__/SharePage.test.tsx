import { createRenderer } from 'react-test-renderer/shallow'
import React from 'react'
import SharePage from '../SharePage'

const shallowRenderer = createRenderer()
shallowRenderer.render(<SharePage />)
const renderedOutput = shallowRenderer.getRenderOutput()

describe('SharePage component', () => {
  it('should render and match the snapshot', () => {
    expect(renderedOutput).toMatchSnapshot()
  })

  it('should includes some child components', () => {
    shallowRenderer.render(<SharePage />)

    expect(renderedOutput.type).toBe('div')
    expect(renderedOutput.props.children.length).toEqual(3)
  })
})
