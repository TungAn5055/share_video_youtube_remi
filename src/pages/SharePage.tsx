import { Container, FormHelperText, makeStyles } from '@material-ui/core'
import React, { useContext, useEffect, useState } from 'react'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { useHistory } from 'react-router-dom'
import Header from '../components/Header'
import { videoYoutubeService } from '../services/videoYoutube.services'
import { Context as LoginContext } from '../contexts/login'
import { EMAIL_LOGIN, TOKEN } from '../ultils/dataUtil'
import { localStorageService } from '../services/localStorage.services'
import { Alert } from '../components/Alert'

const VALIDATION_EMAIL_REGEX = /^(http(s)?:\/\/)[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=.]+$/
const FILL_ALL_INPUT_WARNING = 'Please enter a url!'
const FILL_URL_ERROR = 'Please enter a valid url!'

const useStyles = makeStyles({
  lgCustomInputHint: {
    fontSize: 12,
    lineHeight: '18px',
    fontWeight: 400,
    color: '#DE3131',
    textAlign: 'right',
    paddingLeft: '98px',
  },
  lgCustomInputHintEmail: {
    position: 'absolute',
    // marginTop: '45px',
  },
  lgLoginWarning: {
    color: '#DE3131',
    fontSize: 12,
    fontWeight: 400,
    lineHeight: '18px',
    marginTop: 20,
    textAlign: 'right',
  },
  divContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    padding: '90px',
  },
  fieldSet: {
    maxWidth: '600px',
    padding: '40px 100px 47px 20px',
    margin: '0 auto',
    border: '2px solid #000',
  },
  formUrl: { display: 'flex', flexDirection: 'column', alignItems: 'center' },
  inputForm: {
    marginLeft: '10px',
    borderRadius: 0,
    border: '2px solid #000',
    height: '40px',
    width: '350px',
  },
  buttonForm: {
    boxShadow: 'none',
    borderRadius: 0,
    background: 'transparent',
    color: '#000',
    border: '2px solid #000',
    margin: '20px 0 0 98px',
    width: '350px',
  },
})

const SharePage = (props: any) => {
  const classes = useStyles()
  const history = useHistory()
  const { state } = useContext(LoginContext)
  const [textValue, setTextValue] = useState<string>('')
  const [loginResultMessage, setLoginResultMessage] = useState('')
  const isValidUrl = (url: string) => VALIDATION_EMAIL_REGEX.test(url)
  const isActiveInvalidEmailWarning =
    textValue.length !== 0 && !isValidUrl(textValue)

  const onHandleLoginInputChange = (event: any) => {
    setLoginResultMessage('')
    setTextValue(event.target.value)
  }

  const onSubmitLogin = async (event: any) => {
    event.preventDefault()
    if (textValue === '') {
      setLoginResultMessage(FILL_ALL_INPUT_WARNING)
    } else {
      const successData = await videoYoutubeService.saveVideoYoutubeByUrl(
        textValue,
      )
      if (successData) {
        setTextValue('')
        Alert.success(successData)
      }
    }
  }

  useEffect(() => {
    if (!localStorage?.getItem(TOKEN)) {
      history.push('/')
    }
  }, [state])

  return (
    <>
      <Container maxWidth="lg">
        <Header />
        <div className={classes.divContainer}>
          <div>
            <fieldset className={classes.fieldSet}>
              <legend>
                <Typography component="h1" variant="h5">
                  Share a Youtube movie
                </Typography>
              </legend>

              <form onSubmit={onSubmitLogin} className={classes.formUrl}>
                <div>
                  Youtube URL:
                  <input
                    type="text"
                    name="firstname"
                    value={textValue}
                    onChange={(event: any) => onHandleLoginInputChange(event)}
                    className={classes.inputForm}
                  />
                  {isActiveInvalidEmailWarning && (
                    <FormHelperText
                      className={`${classes.lgCustomInputHint} ${classes.lgCustomInputHintEmail}`}
                    >
                      {FILL_URL_ERROR}
                    </FormHelperText>
                  )}
                  {loginResultMessage.length !== 0 &&
                    !isActiveInvalidEmailWarning && (
                      <FormHelperText
                        className={`${classes.lgCustomInputHint} ${classes.lgCustomInputHintEmail}`}
                      >
                        {FILL_ALL_INPUT_WARNING}
                      </FormHelperText>
                    )}
                  <br />
                  <br />
                </div>

                <Button
                  type="submit"
                  // fullWidth
                  variant="contained"
                  className={classes.buttonForm}
                >
                  Share
                </Button>
              </form>
            </fieldset>
          </div>
        </div>
      </Container>
    </>
  )
}

export default SharePage
