import * as React from 'react'
import Toolbar from '@material-ui/core/Toolbar'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import {
  FormControl,
  FormHelperText,
  IconButton,
  InputAdornment,
  InputLabel,
  makeStyles,
  OutlinedInput,
} from '@material-ui/core'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import CancelIcon from '@material-ui/icons/Cancel'
import { useHistory } from 'react-router-dom'
import { useContext, useEffect, useState } from 'react'
import { Buffer } from 'buffer'
import homeIcon from '../assets/images/homeIcon.png'
import { userService } from '../services/user.services'
import { Context as LoginContext } from '../contexts/login'
import { EMAIL_LOGIN, TOKEN } from '../ultils/dataUtil'
import { localStorageService } from '../services/localStorage.services'
import { Alert } from './Alert'

const useStyles = makeStyles({
  header: {
    borderBottom: '1px',
    borderColor: 'gray',
  },
  title: {
    flex: 1,
    paddingLeft: '13px',
  },
  homeIcon: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
    color: '#474747',
    width: '55px',
  },
  formSubmit: {
    height: '45px',
  },
  formInput: {
    margin: 1,
    width: '25ch',
    flexDirection: 'unset',
    height: '45px',
    marginRight: '10px',
  },
  buttonSubmit: {
    height: '45px',
  },
  paddingLeftHeader: {
    paddingLeft: '5px',
  },
  iconHelperContainer: {
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  helperIconStyle: {
    color: '#8C8C8C',
    width: 20,
    height: 20,
  },
  lgCustomInputHint: {
    fontSize: 12,
    lineHeight: '18px',
    fontWeight: 400,
    color: '#DE3131',
    textAlign: 'right',
  },
  lgCustomInputHintEmail: {
    position: 'absolute',
    marginTop: '45px',
  },
  lgLoginWarning: {
    color: '#DE3131',
    fontSize: 12,
    fontWeight: 400,
    lineHeight: '18px',
    marginTop: 20,
    textAlign: 'right',
  },
})

const EMAIL_PROP_NAME = 'email'
const PASSWORD_PROP_NAME = 'password'
const VALIDATION_EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
const FILL_ALL_INPUT_WARNING = 'Please fill all fields !'

function Header() {
  const classes = useStyles()
  const history = useHistory()
  const { state, updateAfterLogin } = useContext(LoginContext)
  const [loginData, setLoginData] = useState({
    email: '',
    password: '',
  })

  const [typeShowPass, setTypeShowPass] = React.useState<any>(false)
  const [loginResultMessage, setLoginResultMessage] = useState('')
  const isValidEmail = (email: string) => VALIDATION_EMAIL_REGEX.test(email)
  const isActiveInvalidEmailWarning =
    loginData.email.length !== 0 && !isValidEmail(loginData.email)

  const handleClickShowPassword = () => {
    setTypeShowPass(!typeShowPass)
  }

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    event.preventDefault()
  }

  const onHandleLoginInputChange = (event: any, type: string) => {
    setLoginResultMessage('')

    switch (type) {
      case EMAIL_PROP_NAME:
        setLoginData({ ...loginData, email: event.target.value })
        break
      case PASSWORD_PROP_NAME:
        setLoginData({ ...loginData, password: event.target.value })
        break
      default:
    }
  }

  const onHandleClearEmailInput = () => {
    setLoginData({ ...loginData, email: '' })
  }

  const onSubmitLogin = (event: any) => {
    event.preventDefault()
    if (loginData.email === '' || loginData.password === '') {
      setLoginResultMessage(FILL_ALL_INPUT_WARNING)
    } else if (!isActiveInvalidEmailWarning) {
      const messLogin = userService.submitLoginOrRegister(
        loginData.email,
        loginData.password,
      )
      if (messLogin?.status === 'success') {
        Alert.success(messLogin?.message)
        updateAfterLogin(
          Buffer.from(loginData.email + loginData.password).toString('base64'),
          loginData.email,
        )
      } else {
        Alert.error(messLogin?.message)
      }
    }
  }

  useEffect(() => {
    if (localStorage?.getItem(EMAIL_LOGIN) && localStorage?.getItem(TOKEN)) {
      updateAfterLogin(
        localStorageService.getDataLocalStorage(TOKEN),
        localStorageService.getDataLocalStorage(EMAIL_LOGIN),
      )
    }
  }, [])

  return (
    <div>
      <Toolbar className={classes.header}>
        {/* eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions */}
        <img
          src={homeIcon}
          alt="home icon"
          className={classes.homeIcon}
          onClick={() => history.push('/')}
        />

        <Typography
          component="h2"
          variant="h2"
          color="inherit"
          align="left"
          noWrap
          className={classes.title}
          onClick={() => history.push('/')}
        >
          Funny Movies
        </Typography>

        {state?.token ? (
          <>
            <Typography
              component="h5"
              variant="h5"
              color="inherit"
              align="right"
              noWrap
              className={classes.title}
              onClick={() => history.push('/')}
              style={{ marginLeft: '8px' }}
            >
              Welcome {state?.email}
            </Typography>
            <Button
              type="submit"
              variant="outlined"
              size="large"
              className={classes.buttonSubmit}
              style={{ marginLeft: '8px' }}
              onClick={() => {
                history.push('/share')
              }}
            >
              Share a movie
            </Button>
            <Button
              type="button"
              variant="outlined"
              size="large"
              className={classes.buttonSubmit}
              onClick={() => {
                userService?.submitLogout()
                updateAfterLogin('', '')
                setLoginData({
                  email: '',
                  password: '',
                })
              }}
              style={{ marginLeft: '8px' }}
            >
              Logout
            </Button>
          </>
        ) : (
          <form className={classes.formSubmit} onSubmit={onSubmitLogin}>
            <FormControl className={classes.formInput} variant="outlined">
              <InputLabel htmlFor="outlined-adornment">Email</InputLabel>
              <OutlinedInput
                id="outlined-adornment"
                type="text"
                value={loginData.email}
                onChange={(event: any) =>
                  onHandleLoginInputChange(event, EMAIL_PROP_NAME)
                }
                endAdornment={
                  <InputAdornment position="end">
                    {loginData.email.length !== 0 && (
                      <div
                        className={classes.iconHelperContainer}
                        onClick={onHandleClearEmailInput}
                      >
                        <CancelIcon className={classes.helperIconStyle} />
                      </div>
                    )}
                  </InputAdornment>
                }
                label="Email"
              />
              {isActiveInvalidEmailWarning && (
                <FormHelperText
                  className={`${classes.lgCustomInputHint} ${classes.lgCustomInputHintEmail}`}
                >
                  Please enter a valid email
                </FormHelperText>
              )}
            </FormControl>

            <FormControl className={classes.formInput} variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">
                Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={typeShowPass ? 'text' : 'password'}
                value={loginData.password}
                onChange={(event: any) =>
                  onHandleLoginInputChange(event, PASSWORD_PROP_NAME)
                }
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {typeShowPass ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Password"
              />
            </FormControl>

            <Button
              type="submit"
              variant="outlined"
              size="large"
              className={classes.buttonSubmit}
            >
              Login / Register
            </Button>

            {loginResultMessage.length !== 0 &&
              !isActiveInvalidEmailWarning && (
                <FormHelperText className={classes.lgLoginWarning}>
                  {loginResultMessage}
                </FormHelperText>
              )}
          </form>
        )}
      </Toolbar>
      <hr style={{ width: '100%', textAlign: 'center' }} />
    </div>
  )
}

export default Header
