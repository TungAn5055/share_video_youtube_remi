import * as React from 'react'
import {
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  makeStyles,
} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
// eslint-disable-next-line import/no-named-default
import { default as _ReactPlayer } from 'react-player'
import { ReactPlayerProps } from 'react-player/types/lib'
import { videoYoutubeService } from '../services/videoYoutube.services'

const ReactPlayer = (_ReactPlayer as unknown) as React.FC<ReactPlayerProps>

const useStyles = makeStyles({
  list: { width: '100%' },
  divRight: { marginLeft: '30px' },
  pt30: {
    paddingTop: '30px',
  },
  titleH6: { color: 'rgb(198 33 33)', fontWeight: 500 },
})

const ListVideo = () => {
  const classes = useStyles()

  const listVideos = videoYoutubeService.getVideoYoutubeByUrl()

  return (
    <>
      <List className={classes.list}>
        {Array.isArray(listVideos) &&
          listVideos.length > 0 &&
          listVideos.map((item: any) => (
            <div key={item?.idVideo} className={classes.pt30}>
              <ListItem alignItems="flex-start">
                <ListItemAvatar style={{ width: '50%' }}>
                  <ReactPlayer
                    width="100%"
                    height="20em"
                    url={`https://www.youtube.com/watch?v=${item.idVideo}`}
                    controls
                    onError={e => console.log('onError', e)}
                  />
                </ListItemAvatar>
                <div className={classes.divRight}>
                  <Typography
                    variant="h6"
                    component="h6"
                    className={classes.titleH6}
                  >
                    {item.titleVideo}
                  </Typography>
                  <Typography component="h6" variant="h6">
                    Shared by: {item.email}
                  </Typography>
                  <Typography component="h6" variant="h6">
                    Description:
                  </Typography>
                  {item.descriptionVideo && item.descriptionVideo.length > 550
                    ? `${item.descriptionVideo.substr(0, 550)}...`
                    : item.descriptionVideo}
                </div>
              </ListItem>
              <Divider component="li" />
            </div>
          ))}
      </List>
    </>
  )
}

export default ListVideo
