import React from 'react'
import ReactDOM from 'react-dom'
import ListVideo from '../ListVideo'

it('render without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ListVideo />, div)
  ReactDOM.unmountComponentAtNode(div)
})
