import { Buffer } from 'buffer'
import { localStorageService } from './localStorage.services'
import { EMAIL_LOGIN, LOCAL_NAME_DATA_USERS, TOKEN } from '../ultils/dataUtil'
import { handleToken } from '../contexts/login/login.helper'

function submitLoginOrRegister(email: string, password: string) {
  let result: any = { status: '', messType: '' }
  const users = localStorageService.getDataLocalStorage(LOCAL_NAME_DATA_USERS)

  if (Array.isArray(users) && users.length > 0) {
    const userCheck = users.find((item: any) => item?.email === email)
    if (userCheck?.email) {
      // login
      if (userCheck?.password === Buffer.from(password).toString('base64')) {
        result = { status: 'success', message: 'Login Success!' }
        handleToken(userCheck?.token, userCheck?.email)
      } else {
        result = { status: 'error', message: 'Email or Password incorrect.' }
      }
    } else {
      // register
      registerAccount(email, password)
      result = {
        status: 'success',
        message: 'Register account success!',
      }
    }
  } else {
    // register
    registerAccount(email, password)
    result = {
      status: 'success',
      message: 'Register account success!',
    }
  }

  return result
}

function registerAccount(email: string, password: string) {
  const dataLocal =
    localStorageService.getDataLocalStorage(LOCAL_NAME_DATA_USERS) ?? []
  dataLocal.push({
    email,
    password: Buffer.from(password).toString('base64'),
    token: Buffer.from(email + password).toString('base64'),
  })
  localStorageService.setDataLocalStorage(LOCAL_NAME_DATA_USERS, dataLocal)
  handleToken(Buffer.from(email + password).toString('base64'), email)
}

function submitLogout() {
  localStorageService.clearDataLocalStorage(EMAIL_LOGIN)
  localStorageService.clearDataLocalStorage(TOKEN)
}

function getCurrentUser() {
  localStorageService.getDataLocalStorage(EMAIL_LOGIN)
}

export const userService = {
  submitLoginOrRegister,
  submitLogout,
  getCurrentUser,
}
