import {
  API_KEY_YOUTUBE,
  DATA_LIST_VIDEO,
  EMAIL_LOGIN,
} from '../ultils/dataUtil'
import { userService } from './user.services'
import { localStorageService } from './localStorage.services'
import { Alert } from '../components/Alert'

const saveVideoYoutubeByUrl = async (url: string) => {
  let listVideos: any =
    localStorageService.getDataLocalStorage(DATA_LIST_VIDEO) ?? []
  let data: any = {}
  let successData = null

  try {
    if (url) {
      const idVideo = youTubeIdFromLink(url)
      console.log('idVideoidVideoidVideoidVideoidVideo', idVideo)
      if (idVideo) {
        const email = localStorageService.getDataLocalStorage(EMAIL_LOGIN)
        await fetch(
          `https://www.googleapis.com/youtube/v3/videos?key=${API_KEY_YOUTUBE}&fields=items(snippet(title,description),id,statistics(viewCount))&part=snippet,statistics&id=${idVideo}`,
        )
          .then(r => r.json())
          .then(r => {
            if (
              Array.isArray(r?.items) &&
              r?.items.length > 0 &&
              r?.items[0].id
            ) {
              successData = 'Successfully shared video!'
              data = {
                email,
                idVideo: r.items[0]?.id,
                titleVideo: r.items[0]?.snippet?.title,
                descriptionVideo: r.items[0]?.snippet?.description,
              }
              // check duplicate
              if (Array.isArray(listVideos) && listVideos.length > 0) {
                const checkVideo = listVideos.find(
                  (item: any) => item?.idVideo === r.items[0]?.id,
                )

                if (!checkVideo?.idVideo) {
                  listVideos.push(data)
                  localStorageService.setDataLocalStorage(
                    DATA_LIST_VIDEO,
                    listVideos,
                  )
                }
              } else {
                listVideos.push(data)
                localStorageService.setDataLocalStorage(
                  DATA_LIST_VIDEO,
                  listVideos,
                )
              }
            }
          })
      } else {
        Alert.warning('Video link is incorrect')
      }
    }
  } catch (err) {
    Alert.warning('Video link is incorrect')
    console.log(err.message)
  }

  return successData
}

const youTubeIdFromLink = (url: string) => {
  // @ts-ignore
  return url.match(
    /(?:https?:\/\/)?(?:www\.|m\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\/?\?v=|\/embed\/|\/)([^\s&?/#]+)/,
  )[1]
}

const getVideoYoutubeByUrl = () => {
  const listVideo =
    localStorageService.getDataLocalStorage(DATA_LIST_VIDEO) ?? []

  return listVideo
}

export const videoYoutubeService = {
  saveVideoYoutubeByUrl,
  getVideoYoutubeByUrl,
}
