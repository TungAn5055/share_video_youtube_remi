function getDataLocalStorage(name: string) {
  let successData = null
  if (localStorage.getItem(name)) {
    successData = JSON.parse(localStorage.getItem(name) ?? '')
  }

  return successData
}

function setDataLocalStorage(name: string, data: any) {
  localStorage.setItem(name, JSON.stringify(data))
}

function clearDataLocalStorage(name: string) {
  localStorage.removeItem(name)
}

export const localStorageService = {
  getDataLocalStorage,
  setDataLocalStorage,
  clearDataLocalStorage,
}
